<?php

namespace Database\Seeders;

use App\Models\Participant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $participant = [
            [
                'participant_name' => 'Test participant 1',
                'email' => 'admin1@mail.com',
                'status' => '1'
            ],
            [
                'participant_name' => 'Test participant 2',
                'email' => 'admin2@mail.com',
                'status' => '1'
            ],
            [
                'participant_name' => 'Test participant 3',
                'email' => 'admin3@mail.com',
                'status' => '1'
            ],

        ];

        foreach ($participant as $items) {
            Participant::create($items);
        }
    }
}
