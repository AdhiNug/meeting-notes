<?php

namespace Database\Seeders;

use App\Models\MeetingNote;
use Illuminate\Database\Seeder;

class CreateMeetingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $meeting = [
            [
                'category_id' => 1,
                'agenda' => 'Agenda 1',
                'place' => 'Place 1',
                'date' => '2023-07-05',
                'time_start' => '07:54 AM',
                'time_end' => '08:54 AM',
                'type_participant' => 1,
                'content' => '<p>Result 1</p>',
                'note' => 'note 1'
            ],
            [
                'category_id' => 2,
                'agenda' => 'Agenda 2',
                'place' => 'Place 2',
                'date' => '2023-07-06',
                'time_start' => '07:54 AM',
                'time_end' => '08:54 AM',
                'type_participant' => 1,
                'content' => '<p>Result 2</p>',
                'note' => 'note 2'
            ],
            [
                'category_id' => 3,
                'agenda' => 'Agenda 3',
                'place' => 'Place 3',
                'date' => '2023-07-07',
                'time_start' => '07:54 AM',
                'time_end' => '08:54 AM',
                'type_participant' => 1,
                'content' => '<p>Result 3</p>',
                'note' => 'note 3'
            ],
        ];

        foreach($meeting as $items) {
            MeetingNote::create($items);
        }
    }
}
