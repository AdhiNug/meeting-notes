<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CreateCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $categories = [
            [
                'category_name' => 'Project A',
            ],
            [
                'category_name' => 'Project B',
            ],
            [
                'category_name' => 'Project C',
            ],
        ];

        foreach($categories as $items) {
            Category::create($items);
        }
    }
}
