<?php

namespace App\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Contracts\Database\Eloquent\Builder;

class CategoriesFilter extends Filter
{
    /**
     * @param string $title
     * @return Builder
     */

    public function keyword(string $keyword): Builder
    {
        return $this->builder->where('category_name', 'like', '%' . $keyword . '%');
    }
}
