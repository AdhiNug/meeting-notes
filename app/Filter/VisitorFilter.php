<?php

namespace App\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Contracts\Database\Eloquent\Builder;

class VisitorFilter extends Filter
{
    /**
     * @param string $title
     * @return Builder
     */

    public function keyword(string $keyword): Builder
    {
        return $this->builder->where(function ($query) use ($keyword) {
            $query->where('agenda', 'like', '%' . $keyword . '%')
                  ->orWhere('content', 'like', '%' . $keyword . '%')
                  ->orWhere('note', 'like', '%' . $keyword . '%')
                  ->orWhereHas('category', function ($query) use ($keyword) {
                    $query->where('category_name', 'like', '%' . $keyword . '%');
                });
        });
    }
}
