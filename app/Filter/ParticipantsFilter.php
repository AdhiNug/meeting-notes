<?php

namespace App\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ParticipantsFilter extends Filter
{
    /**
     * @param string $title
     * @return Builder
     */

    public function keyword(string $keyword): Builder
    {
        return $this->builder->where('participant_name', 'like', '%' . $keyword . '%');
    }
}
