<?php

namespace App\Filter;

use App\Traits\Filter\Filter;
use Illuminate\Contracts\Database\Eloquent\Builder;

class DashboardsFilter extends Filter
{
    /**
     * @param string $title
     * @return Builder
     */

    public function keyword(string $keyword): Builder
    {
        return $this->builder->where('agenda', 'like', '%' . $keyword . '%');
    }
}
