<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Contracts\LocalizedEnum;
use BenSampo\Enum\Enum;

/**
 * @method static static Inactive()
 * @method static static Active()
 */
final class GeneralStatus extends Enum implements LocalizedEnum
{
    const Active = 1;
    const Inactive = 0;
}
