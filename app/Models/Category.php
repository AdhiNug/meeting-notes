<?php

namespace App\Models;

use App\Traits\Filter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'category_name'
    ];

    public function dashboards()
    {
        return $this->hasMany(Dashboard::class);
    }
}
