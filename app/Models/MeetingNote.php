<?php

namespace App\Models;

use App\Traits\Filter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MeetingNote extends Model
{
    use HasFactory, Filterable;

    protected $fillable = [
        'category_id',
        'agenda',
        'place',
        'date',
        'time_start',
        'time_end',
        'type_participant',
        'participant_list',
        'content',
        'note',
        'file'
    ];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function getCategoryNameAttribute()
    {
        return $this->category->category_name ?? '';
    }
}
