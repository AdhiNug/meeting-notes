<?php

namespace App\Http\Requests\Meeting;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'category' => 'required',
            'agenda' => 'required',
            'place' => 'required',
            'date' => 'required',
            'start_time' => 'nullable',
            'end_time' => 'nullable',
            'participant' => 'required',
            'participant_list' => 'nullable',
            'meeting_result' => 'required',
            'note' => 'nullable',
            'file' => 'nullable'
        ];
    }

    public function dataMeeting()
    {
        $data = [
            'category_id' => $this->category,
            'agenda' => $this->agenda,
            'place' => $this->place,
            'date' => date('Y-m-d', strtotime($this->date)),
            'time_start' => $this->start_time,
            'time_end' => $this->end_time,
            'type_participant' => $this->participant,
            'participant_list' => $this->participant_list,
            'content' => $this->meeting_result,
            'note' => $this->note,
            'file' => $this->file,
        ];

        return $data;
    }
}
