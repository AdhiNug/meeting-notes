<?php

namespace App\Http\Requests\Participant;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'participant_name' => 'required',
            'email' => 'required|email',
            'status' => 'required'
        ];
    }

    public function dataParticipant()
    {
        $data = [
            'participant_name' => $this->participant_name,
            'email' => $this->email,
            'status' => $this->status
        ];

        return $data;
    }
}
