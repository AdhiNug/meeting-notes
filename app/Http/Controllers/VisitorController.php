<?php

namespace App\Http\Controllers;

use App\Filter\VisitorFilter;
use App\Models\MeetingNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class VisitorController extends Controller
{
    public function index(Request $request)
    {
        $meetingNotes = MeetingNote::with('category')
            ->filter(new VisitorFilter($request))
            ->latest()
            ->paginate(Config::get('pagination.default'));

        return view('visitor-page', compact([
            'meetingNotes'
        ]));
    }

    public function show(MeetingNote $meetingNotes)
    {
        return view('visitor-page-detail-search', compact([
            'meetingNotes'
        ]));
    }
}
