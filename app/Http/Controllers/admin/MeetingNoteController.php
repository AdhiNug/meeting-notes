<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use App\Models\MeetingNote;
use Illuminate\Http\Request;
use App\Filter\DashboardsFilter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\Meeting\StoreUpdateRequest;
use App\Mail\mailToParticipant;
use Illuminate\Support\Facades\Mail;

class MeetingNoteController extends Controller
{
    public function index(Request $request)
    {
        $dashboardLists = MeetingNote::with('category')
            ->filter(new DashboardsFilter($request))
            ->latest()
            ->paginate(Config::get('pagination.default'));

        $categories = Category::get();

        return view('pages.dashboards.index', compact(
            'dashboardLists',
            'categories'
        ));
    }

    public function store(StoreUpdateRequest $request)
    {
        MeetingNote::create($request->dataMeeting());

        $mailData = $request->dataMeeting();

        Mail::to('adhi.umby2011@gmail.com')->send(new mailToParticipant($mailData));

        return redirect()->route('dashboard.index')->with('message-success', 'Catatan Meeting baru berhasil ditambahkan');
    }

    public function update(StoreUpdateRequest $request, MeetingNote $meetingNotes)
    {
        $meetingNotes->update($request->dataMeeting());

        return redirect()->route('dashboard.index')->with('message-warning', 'Catatan Meeting berhasil diubah');
    }

    public function destroy(MeetingNote $meetingNotes)
    {
        $meetingNotes->delete();

        return redirect()->route('dashboard.index')->with('message-danger', 'Catatan Meeting berhasil dihapus');
    }
}
