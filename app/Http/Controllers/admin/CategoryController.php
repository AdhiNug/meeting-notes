<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Filter\CategoriesFilter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\Category\StoreUpdateRequest;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::query()
            ->filter(new CategoriesFilter($request))
            ->latest()
            ->paginate(Config::get('pagination.default'));

        return view('pages.categories.index', compact('categories'));
    }

    public function store(StoreUpdateRequest $request)
    {
        Category::create($request->dataCategory());

        return redirect()->route('categories.index')->with('message-success', 'Categori baru berhasil ditambahkan');
    }

    public function update(StoreUpdateRequest $request, Category $categories)
    {
        $categories->update($request->dataCategory());

        return redirect()->route('categories.index')->with('message-warning', 'Categori berhasil diubah');
    }

    public function destroy(Category $categories)
    {
        $categories->delete();

        return redirect()->route('categories.index')->with('message-danger', 'Categori berhasil dihapus');
    }
}
