<?php

namespace App\Http\Controllers\admin;

use App\Filter\ParticipantsFilter;
use App\Models\Participant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Participant\StoreUpdateRequest;
use Illuminate\Support\Facades\Config;

class ParticipantController extends Controller
{
    public function index(Request $request)
    {
        $participants = Participant::query()
            ->filter(new ParticipantsFilter($request))
            ->latest()
            ->paginate(Config::get('pagination.default'));

        return view('pages.participants.index', compact('participants'));
    }

    public function store(StoreUpdateRequest $request)
    {
        Participant::create($request->dataParticipant());

        return redirect()->route('participants.index')->with('message-success', 'Participant baru berhasil ditambahkan');
    }

    public function update(StoreUpdateRequest $request, Participant $participants)
    {
        $participants->update($request->dataParticipant());

        return redirect()->route('participants.index')->with('message-warning', 'Participant berhasil diubah');
    }

    public function destroy(Participant $participants)
    {
        $participants->delete();

        return redirect()->route('participants.index')->with('message-danger', 'Participant berhasil dihapus');
    }
}
