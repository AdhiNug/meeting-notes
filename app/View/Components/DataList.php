<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class DataList extends Component
{
    public $records;

    /**
     * Create a new component instance.
     */
    public function __construct(mixed $records)
    {
        $this->records = $records;
    }

    public function hasPagination(): bool
    {
        return $this->records instanceof LengthAwarePaginator
            && $this->records->hasPages();
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.data-list');
    }
}
