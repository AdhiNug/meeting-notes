<?php

namespace App\Traits\Api;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Building success response
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], $code);
    }

    /**
     * Building success response with message
     * @param $data
     * @param $message
     * @param int $code
     * @return JsonResponse
     */
    public function successResponseMessage($message, array $data = [], $code = Response::HTTP_OK)
    {
        $resultData = [];

        if (!empty($data)) {
            $resultData = [
                'data' => $data
            ];
        }

        return response()->json(array_merge([
            'status' => 'success',
            'message' => $message
        ], $resultData), $code);
    }

    /**
     * @param $message
     * @param $code
     * @return JsonResponse
     */
    public function messageResponse($message, $code = Response::HTTP_OK)
    {
        return response()->json([
            'message' => $message
        ], $code);
    }

    /**
     * @param $message
     * @param $code
     * @return JsonResponse
     */
    public function errorResponse($message, $code = Response::HTTP_UNPROCESSABLE_ENTITY)
    {
        return response()->json([
            'status' => 'error',
            'message' => $message,
            'code' => $code
        ], $code);
    }
}