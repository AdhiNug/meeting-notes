<?php

namespace App\Traits\Others;

use stdClass;

trait ObjectResponser
{
    public function successResponseObject($data)
    {
        $object = new stdClass;
        $object->status = true;
        $object->data = $data;

        return $object;
    }

    public function errorResponseObject(String $message)
    {
        $object = new stdClass;
        $object->status = false;
        $object->message = $message;

        return $object;
    }
}