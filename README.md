# Meeting Notes

The minute of meeting system is an application intended to record all meeting agenda activities so that they can be digitized.

# Table Of Contents

- [Meeting Notes](#meeting-notes)
- [Table Of Contents](#table-of-contents)
  - [Getting Started](#getting-started)
  - [Local Environment](#local-environment)
  - [Development](#development)
    - [Installing](#installing)
    - [User Credential](#user-credential)
    - [Running Vite](#running-vite)
    - [Package And Third Party](#package-and-third-party)
    - [Team Member](#team-member)
  - [Deployment](#deployment)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Local Environment

 - VSCode
 - Composer 2.4
 - Laragon 6.0
 - Nginx 1.16
 - PHP 8.2
 - NodeJS 16
 - MysQL 8.0

## Development
### Installing

A step by step series of examples that tell you how to get a development env running

1.  `$ git clone https://gitlab.com/AdhiNug/meeting-notes.git`
2.  `$ composer install`
3.  Create **.env** file as per **.env.example**. #REQUIRED line must be change
4.  `$ php artisan key:generate`
5.  `$ php artisan storage:link` (Command to generate a symbolic link from public / storage to storage / app / public)
6.  `$ php artisan migrate`
7.  `$ php artisan db:seed`
8.  Set DocumentRoot to {PROJECT_HOME} / public
9.  Access from Browser

### User Credential

1. Super Admin
	- Url login : /login
	- User : admin@aqi.co.id
	- Pass : admin123

### Running Vite

This development with laravel vite to compile asset (including Vue files)
1. `$ npm install`
2. `$ npm run dev` Run the Vite development server
3. `$ npm run build` Build and version the assets for production
4. All asset on /resources/ will compile to /public/build/assets
5. Don't change directly asset (css/js) on /public/build/assets/
### Package And Third Party

1.  Enum : https://github.com/BenSampo/laravel-enum

### Team Member

 - PIC  : Adhi

## Deployment
 - make sure .env set to production and debug set to false.
