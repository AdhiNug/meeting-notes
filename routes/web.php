<?php

use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\admin\MeetingNoteController;
use App\Http\Controllers\admin\ParticipantController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VisitorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [VisitorController::class, 'index'])->name('visitor');
Route::get('visitor-show/{meetingNotes}', [VisitorController::class, 'show'])->name('visitor-show');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    Route::prefix('/participants')->name('participants.')->group(function () {
        Route::get('/', [ParticipantController::class, 'index'])->name('index');
        Route::post('/', [ParticipantController::class, 'store'])->name('store');
        Route::put('/{participants}', [ParticipantController::class, 'update'])->name('update');
        Route::delete('/{participants}', [ParticipantController::class, 'destroy'])->name('delete');
    });

    Route::prefix('/dashboard')->name('dashboard.')->group(function () {
        Route::get('/', [MeetingNoteController::class, 'index'])->name('index');
        Route::post('/', [MeetingNoteController::class, 'store'])->name('store');
        Route::put('/{meetingNotes}', [MeetingNoteController::class, 'update'])->name('update');
        Route::delete('/{meetingNotes}', [MeetingNoteController::class, 'destroy'])->name('delete');
    });

    Route::prefix('categories')->name('categories.')->group(function () {
        Route::get('/', [CategoryController::class, 'index'])->name('index');
        Route::post('/', [CategoryController::class, 'store'])->name('store');
        Route::put('/{categories}', [CategoryController::class, 'update'])->name('update');
        Route::delete('/{categories}', [CategoryController::class, 'destroy'])->name('delete');
    });
});

require __DIR__.'/auth.php';
