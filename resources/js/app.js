import './bootstrap';
import './tabler';
import './timepicker-bs4';
import './datepicker-bs4';
import './custom';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
