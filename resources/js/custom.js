var general = {
    init: function () {
        general.formCategory()
        general.formParticipant()
        general.createMOM()
    },

    formCategory: function () {
        $('#categoryForm').on('submit', function (event) {
            event.preventDefault();

            var categoryId = $('#category_id').val();
            var formData = $(this).serialize();

            var url = '/categories';
            var method = 'POST';

            if (categoryId) {
                url += '/' + categoryId;
                method = 'PUT';
            }

            $.ajax({
                method: method,
                url: url,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#modal-category').modal('hide');
                    location.reload();
                }
            });
        });


        $('#modal-category').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var categoryId = button.data('category-id');
            var categoryName = button.data('category-name');

            var modal = $(this);
            modal.find('.modal-title').text(categoryId ? 'Edit Category' : 'Create New Category');
            modal.find('#category_id').val(categoryId);
            modal.find('#category_name').val(categoryName);
        })
    },

    formParticipant: function () {
        $('#participantForm').on('submit', function (event) {
            event.preventDefault();

            var participantId = $('#participant_id').val();
            var formData = $(this).serialize();

            var url = '/participants';
            var method = 'POST';

            if (participantId) {
                url += '/' + participantId;
                method = 'PUT';
            }

            $.ajax({
                method: method,
                url: url,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#modal-participant').modal('hide');
                    location.reload();
                }
            });
        });

        $('#modal-participant').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var participantId = button.data('participant-id');
            var participantName = button.data('participant-name');
            var participantEmail = button.data('participant-email');
            var participantStatus = button.data('participant-status');

            var modal = $(this);
            modal.find('.modal-title').text(participantId ? 'Edit Participant' : 'Create New Participant');
            modal.find('#participant_id').val(participantId);
            modal.find('#participant_name').val(participantName);
            modal.find('#email').val(participantEmail);

            if (participantStatus === 1) {
                modal.find('#statusActive').prop('checked', true);
            } else {
                modal.find('#statusDeactivated').prop('checked', true);
            }
        })
    },

    createMOM: function () {
        $('#meetingForm').on('submit', function (event) {
            event.preventDefault();

            var meetingId = $('#meeting_id').val();
            var formData = $(this).serialize();

            var url = '/dashboard';
            var method = 'POST';

            if (meetingId) {
                url += '/' + meetingId;
                method = 'PUT';
            }

            $.ajax({
                method: method,
                url: url,
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#modal-meeting').modal('hide');
                    location.reload();
                }
            });
        });

        $('#modal-meeting').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var meetingId = button.data('meeting-id');
            var categoryId = button.data('category-id');
            var agenda = button.data('agenda');
            var place = button.data('place');
            var date = button.data('date');
            var timeStart = button.data('time-start');
            var timeEnd = button.data('time-end');
            var participant = button.data('type-participant');
            var participantList = button.data('participant-list');
            var content = button.data('content');
            var contentText = $('<div>').html(content).text();
            var note = button.data('note');

            var modal = $(this);
            modal.find('.modal-title').text(meetingId ? 'Edit Meeting Notes' : 'Create New Meeting Notes');
            modal.find('#meeting_id').val(meetingId);
            if (categoryId) {
                modal.find('#category').val(categoryId);
            } else {
                modal.find('#category').val();
            }
            modal.find('#agenda').val(agenda);
            modal.find('#place').val(place);
            modal.find('#datepicker').val(date);
            modal.find('#timepicker_start').val(timeStart);
            modal.find('#timepicker_end').val(timeEnd);
            if (participant === 1) {
                modal.find('#privatParticipant').prop('checked', true);
            } else {
                modal.find('#publicParticipant').prop('checked', true);
            }
            modal.find('#participant_name').val(participantList);
            tinymce.get('my-editor').setContent(contentText);
            modal.find('#note_meeting').val(note);
        });

        $(document).on('change', '#privatParticipant', function () {
            var el = $(this).val();

            if (el == 1) {
                $('#participant_list').show();
            } else {
                $('#participant_list').hide();
            }
        });

        $(document).on('change', '#publicParticipant', function () {
            var el = $(this).val();

            if (el == 2) {
                $('#participant_list').hide();
            } else {
                $('#participant_list').show();
            }
        });

        $('#modal-detail-meeting').on('shown.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var categoryName = button.data('category-name');
            var agenda = button.data('agenda');
            var place = button.data('place');
            var date = button.data('date');
            var timeStart = button.data('time-start');
            var timeEnd = button.data('time-end');
            var participant = button.data('type-participant');
            var participantList = button.data('participant-list');
            var content = button.data('content');
            var contentText = $('<div>').html(content).text();
            var note = button.data('note');

            var modal = $(this);
            modal.find('#categoryName').val(categoryName);
            modal.find('#agenda').val(agenda);
            modal.find('#place').val(place);
            modal.find('#date').val(date);
            modal.find('#timepicker_start').val(timeStart);
            modal.find('#timepicker_end').val(timeEnd);
            if (participant === 1) {
                modal.find('#participant').val('Privat');
            } else {
                modal.find('#participant').val('Public');
            }
            tinymce.get('my-editor-show').setContent(contentText);
            modal.find('#note').val(note);
        });
    }
}

$(document).ready(function () {
    general.init();
})
