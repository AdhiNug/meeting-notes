<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>@yield('title', 'Portal Minutes Of Meeting')</title>
    @vite(['resources/css/app.css'])
</head>

<body class=" d-flex flex-column">
    <div>
        <div class="button-login container container-normal py-3 col-6 col-sm-4 col-md-1">
            <a href="{{ route('login') }}" class="btn btn-github w-100 btn-icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon me-1" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M12 7m-4 0a4 4 0 1 0 8 0a4 4 0 1 0 -8 0" />
                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" /></svg>
                Login
            </a>
        </div>
    </div>
    <div class="page-center mt-5">
        <div class="container container-normal py-4">
            <div class="row align-items-center g-4">
                <div class="col-lg">
                    <div class="container-tight">
                        <h1 class="title-app">AQI Minutes Of Meeting</h1>
                        <div class="input-icon">
                            <form>
                                <input type="text" name="keyword" value="{{ request('keyword') }}" class="form-control form-control-rounded" placeholder="Search…">
                                <span class="input-icon-addon">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0" />
                                        <path d="M21 21l-6 -6" /></svg>
                                </span>
                            </form>
                        </div>
                    </div>
                    @if (request('keyword'))
                        @forelse ($meetingNotes as $item)
                            <div class="page-body">
                                <div class="container-xl">
                                    <div class="row row-cards">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="h3 m-0"><a href="{{ route('visitor-show', $item->id) }}">{{ $item->category_name }} - {{ $item->agenda }}</a></div>
                                                    <div class="subheader">{!! $item->content !!}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            @include('components.page-notfound')
                        @endforelse
                    @endif
                </div>
            </div>
        </div>
    </div>
</body>

</html>
