@if (session('message-success'))
    <div class="alert alert-success alert-dismissible fade show border-0" role="alert" id="success-alert">
        {{ session('message-success') }}
    </div>
@endif
@if (session('message-warning'))
    <div class="alert alert-warning alert-dismissible fade show border-0" role="alert" id="warning-alert">
        {{ session('message-warning') }}
    </div>
@endif
@if (session('message-danger'))
    <div class="alert alert-danger alert-dismissible fade show border-0" role="alert" id="danger-alert">
        {{ session('message-danger') }}
    </div>
@endif

<script>
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#success-alert").slideUp(500);
    });
    $("#warning-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#warning-alert").slideUp(500);
    });
    $("#danger-alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#danger-alert").slideUp(500);
    });
</script>
