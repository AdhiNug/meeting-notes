<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>@yield('title', 'Portal Minutes Of Meeting')</title>
    <link href="{{ asset('libs/tinymce/js/tinymce/skins/ui/oxide/skin.min.css') }}" rel="stylesheet">
    <script src="{{ asset('libs/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    @vite(['resources/css/app.css'])
</head>

<body class=" d-flex flex-column">
    <div>
        <div class="button-login container container-normal py-3 col-6 col-sm-4 col-md-1">
            <a href="{{ route('login') }}" class="btn btn-github w-100 btn-icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon me-1" width="24" height="24" viewBox="0 0 24 24"
                    stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M12 7m-4 0a4 4 0 1 0 8 0a4 4 0 1 0 -8 0" />
                    <path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" /></svg>
                Login
            </a>
        </div>
    </div>
    <div class="page-center mt-5">
        <div class="container container-normal py-4">
            <div class="row align-items-center g-4">
                <div class="col-lg">
                    <div class="container-tight">
                        <h1 class="title-app">Detail Minutes Of Meeting</h1>
                        <div class="input-icon">
                        </div>
                    </div>
                    <div class="container-xl">
                        <div class="card">
                            <div class="card-body">
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Category</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ $meetingNotes->category_name ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Agenda</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ $meetingNotes->agenda ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Place</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ $meetingNotes->place ?? '' }}"disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Date</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ $meetingNotes->date ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Time meeting</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-3">
                                        <input class="form-control col-8" value="{{ $meetingNotes->time_start ?? '' }}" disabled>
                                    </div>
                                    <label class="col-2 col-form-label text-center">Until</label>
                                    <div class="col-3">
                                        <input class="form-control col-8" value="{{ $meetingNotes->time_end ?? '' }}" disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Participant</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ ($meetingNotes->type_participant === 1) ? 'Privat' : 'Public' }}" disabled>
                                    </div>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Participant list</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" id="" disabled>
                                    </div>
                                </div>

                                <div class="mb-3">
                                    <label class="form-label">Result of Meeting</label>
                                    <textarea id="my-editor-show" name="meeting_result">{{ $meetingNotes->content ?? '' }}</textarea>
                                </div>
                                <div class="mb-3 row">
                                    <label class="col-3 col-form-label">Note</label>
                                    <label class="col-1 col-form-label">:</label>
                                    <div class="col-8">
                                        <input class="form-control col-8" value="{{ $meetingNotes->note ?? '' }}" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer mt-3">
                            <a href="{{ url()->previous() }}" class="btn btn-danger" data-bs-dismiss="modal">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-arrow-back-up-double" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M13 14l-4 -4l4 -4"></path>
                                    <path d="M8 14l-4 -4l4 -4"></path>
                                    <path d="M9 10h7a4 4 0 1 1 0 8h-1"></path>
                                 </svg>
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>

<script>
    tinymce.init({
            selector: '#my-editor-show',
            height: 300,
            menubar: true,
            statusbar: false,
            readonly: true
        });
</script>
