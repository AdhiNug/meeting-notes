@extends('apps.layouts.app')

@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <div class="page-pretitle">
                    list
                </div>
                <h2 class="page-title">
                    Minute Of Meeting
                </h2>
            </div>
            <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                    <a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal"
                        data-bs-target="#modal-meeting">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M12 5l0 14" />
                            <path d="M5 12l14 0" /></svg>
                        Create new Minute Of Meeting
                    </a>
                    <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal"
                        data-bs-target="#modal-meeting" aria-label="Create new report">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M12 5l0 14" />
                            <path d="M5 12l14 0" /></svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="container-xl">
        <div class="col-7 mb-3">
            <div class="row g-2">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Search for…">
                </div>
                <div class="col-2">
                    <a href="#" class="btn btn-info w-100">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0" />
                            <path d="M21 21l-6 -6" />
                        </svg>
                        search
                    </a>
                </div>
                <div class="col-2">
                    <a href="#" class="btn btn-danger w-100">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-reload" width="24"
                            height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M19.933 13.041a8 8 0 1 1 -9.925 -8.788c3.899 -1 7.935 1.007 9.425 4.747"></path>
                            <path d="M20 4v5h-5"></path>
                        </svg>
                        Reset
                    </a>
                </div>
            </div>
        </div>
        <div class="row row-deck row-cards">
            <div class="col-12">
                <div class="card">
                    <x-data-list :records="$dashboardLists">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="15%">Category</th>
                                    <th width="40%">Agenda</th>
                                    <th width="15%">Place</th>
                                    <th width="15%">Date</th>
                                    <th width="15%">
                                        <center>Action</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($dashboardLists as $dashboardList)
                                    <tr>
                                        <td>{{ $dashboardList->category_name ?? '' }}</td>
                                        <td>{{ $dashboardList->agenda ?? '' }}</td>
                                        <td>{{ $dashboardList->place ?? '' }}</td>
                                        <td>{{ date('d-m-Y', strtotime($dashboardList->date)) ?? '' }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <button class="btn btn-outline-secondary w-100" data-bs-toggle="modal" data-bs-target="#modal-detail-meeting"
                                                    data-category-name="{{ $dashboardList->category_name }}"
                                                    data-agenda="{{ $dashboardList->agenda }}"
                                                    data-place="{{ $dashboardList->place }}"
                                                    data-date="{{ date('m/d/Y', strtotime($dashboardList->date)) }}"
                                                    data-time-start="{{ $dashboardList->time_start }}"
                                                    data-time-end="{{ $dashboardList->time_end }}"
                                                    data-type-participant="{{ $dashboardList->type_participant }}"
                                                    data-content="{{ $dashboardList->content }}"
                                                    data-note="{{ $dashboardList->note }}"
                                                    >
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        class="icon icon-tabler icon-tabler-eye-check" width="24" height="24"
                                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                                        stroke-linecap="round" stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                        <path d="M10 12a2 2 0 1 0 4 0a2 2 0 0 0 -4 0"></path>
                                                        <path
                                                            d="M11.143 17.961c-3.221 -.295 -5.936 -2.281 -8.143 -5.961c2.4 -4 5.4 -6 9 -6c3.6 0 6.6 2 9 6c-.222 .37 -.449 .722 -.68 1.057">
                                                        </path>
                                                        <path d="M15 19l2 2l4 -4"></path>
                                                    </svg>
                                                    Detail
                                                </button>
                                                <button class="btn btn-outline-success w-100" data-bs-toggle="modal" data-bs-target="#modal-meeting"
                                                    data-meeting-id="{{ $dashboardList->id }}"
                                                    data-category-id="{{ $dashboardList->category_id }}"
                                                    data-agenda="{{ $dashboardList->agenda }}"
                                                    data-place="{{ $dashboardList->place }}"
                                                    data-date="{{ date('m/d/Y', strtotime($dashboardList->date)) }}"
                                                    data-time-start="{{ $dashboardList->time_start }}"
                                                    data-time-end="{{ $dashboardList->time_end }}"
                                                    data-type-participant="{{ $dashboardList->type_participant }}"
                                                    data-content="{{ $dashboardList->content }}"
                                                    data-note="{{ $dashboardList->note }}"
                                                    >
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        class="icon icon-tabler icon-tabler-edit" width="24" height="24"
                                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                                        stroke-linecap="round" stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                        <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1">
                                                        </path>
                                                        <path
                                                            d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z">
                                                        </path>
                                                        <path d="M16 5l3 3"></path>
                                                    </svg>
                                                    Edit
                                                </button>
                                                <form action="{{ route('dashboard.delete', $dashboardList->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-outline-danger w-100" type="submit" onclick="return confirm('Apakah Anda yakin ingin menghapus catatan meeting ini?')">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="icon icon-tabler icon-tabler-trash-x" width="24" height="24"
                                                            viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                                            stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <path d="M4 7h16"></path>
                                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                                            <path d="M10 12l4 4m0 -4l-4 4"></path>
                                                        </svg>
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2">Data masih kosong</td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </x-data-list>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-blur fade" id="modal-meeting" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Minute of Meeting</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="meetingForm" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="meeting_id">
                <div class="modal-body">
                    <div class="col-lg-6 mb-3">
                        <label class="form-label label-mandatory">Category</label>
                        <select class="form-select" id="category" name="category">
                            <option>Select Category</option>
                            @foreach ($categories as $item)
                                <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label class="form-label label-mandatory">Agenda</label>
                        <input type="text" class="form-control" id="agenda" name="agenda" placeholder="Entry Agenda">
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label class="form-label label-mandatory">Place</label>
                        <input type="text" class="form-control" id="place" name="place" placeholder="Entry Place">
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label class="form-label label-mandatory">Date</label>
                    </div>
                    <div class="col-lg-6 input-icon mb-3">
                        <input type="text" id="datepicker" class="form-control datepicker" name="date" placeholder="Select a date" />
                        <span class="input-icon-addon">
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 5m0 2a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2z" /><path d="M16 3l0 4" /><path d="M8 3l0 4" /><path d="M4 11l16 0" /><path d="M11 15l1 0" /><path d="M12 15l0 3" /></svg>
                        </span>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <label class="form-label label-mandatory">Time meeting</label>
                        <div class="row">
                            <div class="col-6">
                                <input type="text" id="timepicker_start" class="form-control timepicker-input" name="start_time" value="" size="10" autocomplete="off" placeholder="Start"/>
                            </div>
                            <div class="col-6">
                                <input type="text" id="timepicker_end" class="form-control timepicker-input" name="end_time" value="" size="10" autocomplete="off" placeholder="End"/>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="form-label label-mandatory">Participant</div>
                        <div>
                            <label class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="participant" id="privatParticipant" value="1" {{ old('participant') === 1 ? 'checked' : '' }} checked>
                                <span class="form-check-label">Privat</span>
                            </label>
                            <label class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="participant" id="publicParticipant" value="2" {{ old('participant') === 2 ? 'checked' : '' }}>
                                <span class="form-check-label">Public</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-3">
                        <input type="text" id="participant_list" class="form-control" name="participant_list" placeholder="Entry Participant">
                    </div>
                    <div class="mb-3">
                        <label class="form-label label-mandatory">Result of Meeting</label>
                            <textarea id="my-editor" name="meeting_result"></textarea>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Note</label>
                        <input type="text" id="note_meeting" class="form-control" name="note" placeholder="Entry Note. Ex : next agenda is...">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Upload File</label>
                        <input type="file" class="form-control" name="file">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-danger" data-bs-dismiss="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M18 6l-12 12"></path>
                            <path d="M6 6l12 12"></path>
                        </svg>
                        Cancel
                    </a>
                    <button type="submit" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-device-floppy"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <path d="M12 14m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path>
                            <path d="M14 4l0 4l-6 0l0 -4"></path>
                        </svg>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal modal-blur fade" id="modal-detail-meeting" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Minute of Meeting</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form>
                <input type="hidden" id="meeting_id">
                <div class="modal-body">
                    <div class="card-body">
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Category</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="categoryName" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Agenda</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="agenda" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Place</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="place" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Date</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="date" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Time meeting</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-3">
                                <input class="form-control col-8" id="timepicker_start" disabled>
                            </div>
                            <label class="col-2 col-form-label text-center">Until</label>
                            <div class="col-3">
                                <input class="form-control col-8" id="timepicker_end" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Participant</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="participant" disabled>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Participant list</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="" disabled>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label">Result of Meeting</label>
                            <textarea id="my-editor-show" name="meeting_result"></textarea>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-3 col-form-label">Note</label>
                            <label class="col-1 col-form-label">:</label>
                            <div class="col-8">
                                <input class="form-control col-8" id="note" disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-danger" data-bs-dismiss="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-x" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M18 6l-12 12"></path>
                            <path d="M6 6l12 12"></path>
                        </svg>
                        Close
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>

<link href="{{ asset('libs/tinymce/js/tinymce/skins/ui/oxide/skin.min.css') }}" rel="stylesheet">
<script src="{{ asset('libs/tinymce/js/tinymce/tinymce.min.js') }}"></script>

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // time picker
        var $picker = jQuery('#timepicker_start').timepicker();
        var $picker = jQuery('#timepicker_end').timepicker();

        // date picker
	    var $picker = jQuery('#datepicker').datepicker();

        // tiny MCE
        tinymce.init({
            selector: '#my-editor',
            height: 300,
            menubar: true,
            statusbar: false,
        });

        tinymce.init({
            selector: '#my-editor-show',
            height: 300,
            menubar: true,
            statusbar: false,
            readonly: true
        });
    });
</script>
@endsection
