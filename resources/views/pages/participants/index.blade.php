@extends('apps.layouts.app')

@section('content')
<div class="page-header d-print-none">
    <div class="container-xl">
        <div class="row g-2 align-items-center">
            <div class="col">
                <div class="page-pretitle">
                    Master Data
                </div>
                <h2 class="page-title">
                    Participants
                </h2>
            </div>
            <div class="col-auto ms-auto d-print-none">
                <div class="btn-list">
                    <a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal"
                        data-bs-target="#modal-participant">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M12 5l0 14" />
                            <path d="M5 12l14 0" /></svg>
                        Create new participant
                    </a>
                    <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal"
                        data-bs-target="#modal-participant" aria-label="Create new report">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                            stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                            stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                            <path d="M12 5l0 14" />
                            <path d="M5 12l14 0" /></svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-body">
    <div class="container-xl">
        <div class="col-7 mb-3">
            <form>
                <div class="row g-2">
                    <div class="col">
                        <input type="text" name="keyword" class="form-control" placeholder="Search for…" value="{{ request('keyword') }}">
                    </div>
                    <div class="col-2">
                        <button type="submit" class="btn btn-info w-100 d-none d-sm-inline-block">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24"
                                stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0" />
                                <path d="M21 21l-6 -6" />
                            </svg>
                            search
                        </button>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('participants.index') }}" class="btn btn-danger w-100">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-reload" width="24"
                                height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M19.933 13.041a8 8 0 1 1 -9.925 -8.788c3.899 -1 7.935 1.007 9.425 4.747"></path>
                                <path d="M20 4v5h-5"></path>
                            </svg>
                            Reset
                        </a>
                    </div>
                </div>
            </form>
        </div>
        @include('apps.components.app-message')
        <div class="row row-deck row-cards">
            <div class="col-12">
                <div class="card">
                    <x-data-list :records="$participants">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th width="50%">Participant Name</th>
                                    <th width="20%">Email</th>
                                    <th width="15%">Status</th>
                                    <th width="15%">
                                        <center>Action</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($participants as $participant)
                                    <tr>
                                        <td>{{ $participant->participant_name ?? '' }}</td>
                                        <td>{{ $participant->email ?? '' }}</td>
                                        <td>
                                            @if ($participant->status == App\Enums\GeneralStatus::Active)
                                                <span class="badge bg-green-lt">Active</span>
                                            @else
                                                <span class="badge bg-red-lt">Deactivate</span>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <button class="btn btn-outline-success w-100" data-bs-toggle="modal" data-bs-target="#modal-participant"
                                                    data-participant-id="{{ $participant->id }}"
                                                    data-participant-name="{{ $participant->participant_name }}"
                                                    data-participant-email="{{ $participant->email }}"
                                                    data-participant-status="{{ $participant->status }}"
                                                    >
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        class="icon icon-tabler icon-tabler-edit" width="24" height="24"
                                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                                        stroke-linecap="round" stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                        <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1">
                                                        </path>
                                                        <path
                                                            d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z">
                                                        </path>
                                                        <path d="M16 5l3 3"></path>
                                                    </svg>
                                                    Edit
                                                </button>
                                                <form action="{{ route('participants.delete', $participant->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-outline-danger w-100" type="submit" onclick="return confirm('Apakah Anda yakin ingin menghapus participant ini?')">
                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                            class="icon icon-tabler icon-tabler-trash-x" width="24" height="24"
                                                            viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                                            stroke-linecap="round" stroke-linejoin="round">
                                                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                                            <path d="M4 7h16"></path>
                                                            <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                                            <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                                                            <path d="M10 12l4 4m0 -4l-4 4"></path>
                                                        </svg>
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4">Data masih kosong</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </x-data-list>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal-blur fade" id="modal-participant" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="participantForm" method="POST">
                @csrf
                <input type="hidden" id="participant_id">
                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label label-mandatory">Participant Name</label>
                        <input type="text" class="form-control" id="participant_name" name="participant_name" placeholder="Participant name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label label-mandatory">Email</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="Email Participant">
                    </div>
                    <div class="mb-3">
                        <div class="form-label label-mandatory">Status</div>
                        <div>
                            <label class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="statusActive" name="status" value="1" {{ old('status') === 1 ? 'checked' : '' }} checked>
                                <span class="form-check-label">Active</span>
                            </label>
                            <label class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" id="statusDeactivated" name="status" value="0" {{ old('status') === 0 ? 'checked' : '' }}>
                                <span class="form-check-label">Deactivated</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-link link-secondary" data-bs-dismiss="modal">
                        Cancel
                    </a>
                    <button type="submit" class="btn btn-primary ms-auto" data-bs-dismiss="modal">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-device-floppy"
                            width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                            stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <path d="M12 14m-2 0a2 2 0 1 0 4 0a2 2 0 1 0 -4 0"></path>
                            <path d="M14 4l0 4l-6 0l0 -4"></path>
                        </svg>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
