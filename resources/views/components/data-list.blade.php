<div>
    <div>
        {{ $slot }}
    </div>

    @if ($hasPagination && $records->total() > 0)
        <div>
            {{ $records->links('components.data-pagination') }}
        </div>
    @endif
</div>
