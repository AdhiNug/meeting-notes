<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail To Participant</title>
</head>
<body>
    <table style="width: 100%;" border="0">
        <tbody>
            <tr>
                <td>Category</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['category_id'] }}</td>
            </tr>
            <tr>
                <td>Agenda</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['agenda'] }}</td>
            </tr>
            <tr>
                <td>Place</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['place'] }}</td>
            </tr>
            <tr>
                <td>Date</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['date'] }}</td>
            </tr>
            <tr>
                <td>Time meeting</td>
                <td>:</td>
                <td>{{ $mailData['time_start'] }}</td>
                <td>Until</td>
                <td>{{ $mailData['time_end'] }}</td>
            </tr>
            <tr>
                <td>Participant</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['type_participant'] === 1 ? 'Privat' : 'Public' }}</td>
            </tr>
            <tr>
                <td>Participant List</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['participant_list'] }}</td>
            </tr>
            <tr>
                <td>Result of Meeting</td>
                <td>:</td>
                <td colspan="3">{!! $mailData['content'] !!}</td>
            </tr>
            <tr>
                <td>Note</td>
                <td>:</td>
                <td colspan="3">{{ $mailData['note'] }}</td>
            </tr>
        </tbody>
    </table>
</body>
</html>
